# vi: set ft=ruby :

Vagrant.require_version ">= 1.5.0"
require 'vagrant-hosts'
require 'vagrant-auto_network'

VAGRANTFILE_API_VERSION = "2"

nodes_config = (JSON.parse(File.read("VagrantNodes.json")))['nodes']

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
 
  nodes_config.each do |node|
    node_name   = node[0] # name of node
    node_values = node[1] # content of node
 
    config.vm.define node_name do |node|
    
      node.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", node_values[':memory']]
        vb.customize ["modifyvm", :id, "--name", node_name]
      end
    
      node.vm.box = node_values[':boxname']
      node.vm.box_url = node_values[':boxurl']
      node.vm.hostname = node_name
      node.vm.network :private_network, :auto_network => true
      node.vm.provision :hosts

      ports = node_values['ports']
      ports.each do |port|
        node.vm.network :forwarded_port,
          host:  port[":host"],
          guest: port[":guest"]
      end
            
      scripts = node_values["scripts"]
      scripts.each do |script|
        node.vm.provision :shell, :path => script
      end
      
    end
  end
end
